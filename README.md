## Asset Description :
- This asset contains a method `updateCartItems` in apex class `CartActionController` which can be used to perform cart actions such as ADD , DELETE , UPDATE on cart items .
- This apex class can be used in both LWC and Aura components.
- This has been used in project 7s:nVent B2B Commerce Cloud Implementation.

## Asset Use :
- This method can be used to perform cart actions.
- Parameters required to pass are - Action Name , List of record Id to perform action on , New quantity of Cart Item.
- Simply invoke the updateCartItems() method with the necessary parameters mentioned above from LWC/Aura component JavaScript which will return the updated CartModel.