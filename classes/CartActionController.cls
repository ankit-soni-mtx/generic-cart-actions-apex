/**
 * Created By ankitSoni on 28/08/2020
 */
public class CartActionController {
    
    public static final String CART_UPDATE = 'update';
	public static final String CART_ADD    = 'add';
	public static final String CART_DELETE = 'delete';
    
    /**
	 * Description : Method performing Cart Actions (add/delete/update)
	 * @param : action , itemList(sfid) , newQuantity
	 * @return : Updated Cart Model
	 */
    @AuraEnabled
	public static CartModel updateCartItems(String action,List<String> itemList, String newQuantity){
		
		//get current cart
		CartModel model = CurrentCartHandler.getCurrentCart();
		Boolean success = false;

		System.debug('updateCartItems: ' + action + ',itemList ' + itemList+ ',newQuantity ' + newQuantity);

		List<Map<String, Object>> newLineItems = new List<Map<String, Object>>();
		List<ccrz.ccApiCart.LineData> oldLineItems = new List<ccrz.ccApiCart.LineData>();

		Map<String, Object> inputData = new Map<String, Object>{
			ccrz.ccApi.API_VERSION    => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccApiCart.CART_ENCID => model.cartEncId
		};

		//Prepare input Data for delete//
		if (action == CART_DELETE || action == CART_UPDATE){
			if (itemList != null && itemList.size() > 0){
				for (String item : itemList){
					ccrz.ccApiCart.LineData lineItem = new ccrz.ccApiCart.LineData();
					lineItem.sfid = item;
					oldLineItems.add(lineItem);
				}
				System.debug('Old line items: ' + oldLineItems);
			}
		}

		//Prepare input Data for Add or Update//
		if (action == CART_ADD || action == CART_UPDATE){
			if (itemList != null && itemList.size() > 0){
				Integer pos = 0;
				for (String item : itemList){
					newLineItems.add(new Map<String, Object>{
						ccrz.ccApiCart.LINE_DATA_PRODUCT_SFID => Id.valueOf(item),
						ccrz.ccApiCart.LINE_DATA_QUANTITY => Integer.valueOf(newQuantity)
					});
				}
				System.debug('New line items: ' + newLineItems);
			}
		}

		//Perform Delete Action
		if (action == CART_DELETE || action == CART_UPDATE){
			inputData.put(ccrz.ccApiCart.LINE_DATA, oldLineItems);

			System.debug('Remove Old items: ' + inputData);

			Map<String, Object> removeResults = ccrz.ccApiCart.removeFrom(inputData);
			success = (Boolean) removeResults.get(ccrz.ccApi.SUCCESS);

			System.debug('Remove old items: ' + success);
			System.debug('Remove messages : ' + (List<ccrz.cc_bean_Message>) removeResults.get(ccrz.ccApi.MESSAGES));
		}

		//Perform Add/Update Action
		if (action == CART_ADD || action == CART_UPDATE){
			inputData.put(ccrz.ccApiCart.ISREPRICE,true);
			inputData.put(ccrz.ccApiCart.LINE_DATA, newLineItems);

			System.debug('Add/Update input: ' + inputData);

			Map<String, Object> addResults = ccrz.ccApiCart.addTo(inputData);
			success = (Boolean) addResults.get(ccrz.ccApi.SUCCESS);

			System.debug('Add/Update items   : ' + success);
			System.debug('Add/Update messages: ' + (List<ccrz.cc_bean_Message>)addResults.get(ccrz.ccApi.MESSAGES));
		}

		if (success){
			System.debug('Cart Action :'+action+ 'SUCCESS');
			model = CurrentCartHandler.getCurrentCart();
		}

		return model;

	}
    
}
