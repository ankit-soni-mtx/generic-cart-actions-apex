/**
 * Created By ankitSoni on 27/08/2020
 */
public class CartModel {
    
    @AuraEnabled
    public String cartId,cartEncId,currencyISOCode;

	@AuraEnabled
	public Decimal totalQuantity, totalPrice, itemCount;

	@AuraEnabled
	public List<CartItemModel> items;

    public CartModel() {
        cartId = '';
		cartEncId = '';
		items  = new List<CartItemModel>();
    }

}